/**
 * Created by u5923103 on 27/02/17.
 */
public class maxmin {
    public static int Max(int[] Array){
        int maxValue = Array[Array.length-1];
        for(int i=1;i < Array.length;i++){
            if(Array[i] > maxValue){
                maxValue = Array[i];
            }
        }
        return maxValue;
    }

    // Method for getting the minimum value
    public static int Min(int[] Array){
        int minValue = Array[Array.length-1];
        for(int i=1;i<Array.length;i++){
            if(Array[i] < minValue){
                minValue = Array[i];
            }
        }
        return minValue;
    }
}
